import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { AppComponent } from './app.component';
import { ButtonComponent } from './button/button.component';
import { StyledButtonComponent } from './styled-button/styled-button.component';

@NgModule({ 
  declarations: [
    AppComponent,
    ButtonComponent,
    StyledButtonComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [ButtonComponent, StyledButtonComponent]
})
export class AppModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap() {
    const customElement = createCustomElement(
      ButtonComponent, { injector: this.injector }
    );

    const styledButton = createCustomElement(
      StyledButtonComponent, { injector: this.injector }
    )

    customElements.define('custom-button', customElement);
    customElements.define('styled-button', styledButton);
  }
}
