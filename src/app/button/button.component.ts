import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class ButtonComponent implements OnInit {
  @Input() label;
  @Output() clicked = new EventEmitter();

  private noOfClicks = 0;
  constructor() { }

  onClick() {
    this.noOfClicks++;
    this.clicked.emit(this.noOfClicks);
  }

  ngOnInit() { }

}
