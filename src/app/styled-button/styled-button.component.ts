import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-styled-button',
  templateUrl: './styled-button.component.html',
  styleUrls: ['./styled-button.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class StyledButtonComponent implements OnInit {
  @Input() label;
  @Output() clicked = new EventEmitter();

  private noOfClicks = 0;
  constructor() { }

  handleClick() {
    this.noOfClicks++;
    this.clicked.emit(this.noOfClicks);
  }

  ngOnInit() { }

}
